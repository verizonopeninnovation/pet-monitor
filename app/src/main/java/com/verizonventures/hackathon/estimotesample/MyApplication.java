package com.verizonventures.hackathon.estimotesample;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.TextView;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.verizon.oi.Dweet;
import com.verizon.oi.WemoInsight;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by piyush on 10/8/15.
 */
public class MyApplication extends Application {

    private BeaconManager beaconManager;
    private String TAG = "PetMonitor";

    @Override
    public void onCreate() {
        super.onCreate();

        final WemoInsight mWemoInsight = new WemoInsight(this);
        beaconManager = new BeaconManager(getApplicationContext());

        beaconManager.setMonitoringListener(new BeaconManager.MonitoringListener() {
            @Override
            public void onEnteredRegion(Region region, List<Beacon> list) {
                //Action that happens when pet enters region
                mWemoInsight.setState(1);
                Log.d(TAG, "onEnteredRegion() called");
                String payload = "{\"Event\" : \"Entered\" , " + "\"Date\" : \"" + getReadableDate() + "\"}";
                Dweet.send(payload, getApplicationContext());
            }
            @Override
            public void onExitedRegion(Region region) {
                // Action that happens when the pet exits region
                mWemoInsight.setState(0);
                String payload = "{\"Event\" : \"Exited\" , " + "\"Date\" : \"" + getReadableDate() + "\"}";
                Dweet.send(payload, getApplicationContext());
                Log.d(TAG, "Exited : " + getReadableDate());
            }
        });

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                beaconManager.startMonitoring(new Region(
                        "monitored region",
                        UUID.fromString("B9407F30-F5F8-466E-AFF9-25556B57FE6D"),
                        2960, 40441));
            }
        });

    }

    public String getReadableDate () {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");
        Date dt = new Date();
        return sdf.format(dt);
    }


}
