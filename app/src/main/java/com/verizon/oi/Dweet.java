package com.verizon.oi;

import android.content.Context;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

/**
 * Created by piyush on 10/29/15.
 */
public class Dweet {

    private static String url="http://dweet.io/dweet/for/oiPetMonitor";
    private String mRequest = "{\"luminosity\" : \"400\"}";
    private static String TAG = "dweet";

    public static void send(String mRequest, Context context){
        JsonObjectRequest jsonRequest = new JsonObjectRequest
                (Request.Method.POST, url, mRequest, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // the response is already constructed as a JSONObject
                        Log.d(TAG, response.toString());
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });

        Volley.newRequestQueue(context).add(jsonRequest);

    }
}
